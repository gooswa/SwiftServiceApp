//
//  MainController.swift
//  SwiftServiceApp
//
//  Created by Justin Gaussoin on 2/13/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import Cocoa

class MainController: NSObject, StatusItemDelegate {
    
    
    var window:CustomWindow?
    var statusItem:NSStatusItem?
    var statusItemView:CustomStatusItem?
    
    @IBOutlet weak var view:NSView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let win = CustomWindow(theView: view)
        win.collectionBehavior = NSWindowCollectionBehavior.CanJoinAllSpaces
        
        let sitem:NSStatusItem = NSStatusBar.systemStatusBar().statusItemWithLength(-1)
        
        var height = NSStatusBar.systemStatusBar().thickness
        var width = height
        var statusItemFrame = NSMakeRect(0, 0, width, height)
        
        var sitemView:CustomStatusItem = CustomStatusItem(frame: statusItemFrame)
        sitem.view = sitemView
        
        sitemView.delegate = self
        
        window = win
        statusItem = sitem
        statusItemView = sitemView
    }
    
    
    func toggleWindow(statusItem: CustomStatusItem) {
        
        var statusRect:NSRect = statusItem.window!.convertRectToScreen(statusItem.frame)
        if let win = window {
            NSApp.activateIgnoringOtherApps(true)
            win.setAnchorPoint(statusRect)
            win.toggleVisibility()
            
        }
        
        
    }
}
