//
//  CustomWindow.swift
//  SwiftServiceApp
//
//  Created by Justin Gaussoin on 2/13/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import Cocoa

class CustomWindow: NSWindow, NSWindowDelegate {
    
    override var canBecomeKeyWindow:Bool {
        return true
    }
    
    override dynamic var alphaValue:CGFloat {
        get {
            return super.alphaValue
        }
        set {
            super.alphaValue = newValue
            if newValue == 0 {
                self.orderOut(self)
            } else {
                self.makeKeyAndOrderFront(self)
            }
        }
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(contentRect: NSRect, styleMask aStyle: Int, backing bufferingType: NSBackingStoreType, defer flag: Bool) {
        super.init(contentRect: contentRect, styleMask: aStyle, backing: bufferingType, defer: flag)
    }
    
    convenience init(theView:NSView) {
        var contentRect:NSRect = NSZeroRect
        contentRect.size = theView.frame.size
        self.init(contentRect: contentRect, styleMask: NSBorderlessWindowMask, backing: NSBackingStoreType.Buffered, defer: false)
        
        self.contentView.addSubview(theView)
        self.excludedFromWindowsMenu = true
        self.opaque = false
        self.hasShadow = true
        self.delegate = self
        
    }
    
    

    func setAnchorPoint(statusRect:NSRect) {
        var xPoint = round(NSMidX(statusRect) - NSWidth(self.frame) / 2);
        var yPoint = NSMinY(statusRect) - NSHeight(self.frame);
        var newRect = NSRect(origin: CGPoint(x: xPoint, y: yPoint), size: self.frame.size)
        self.setFrame(newRect, display: true)
    }
    
    func windowDidResignKey(notification: NSNotification) {
        self.animator().alphaValue = 0
    }
    
    func toggleVisibility() {

        
        if !self.visible {
            self.alphaValue = 0
            self.animator().alphaValue = 1.0
        } else {
            self.animator().alphaValue = 0.0
        }
        
    }
}


