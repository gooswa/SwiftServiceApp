//
//  CustomStatusItem.swift
//  SwiftServiceApp
//
//  Created by Justin Gaussoin on 2/13/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import Cocoa

protocol StatusItemDelegate {
    func toggleWindow(statusItem:CustomStatusItem);
}

class CustomStatusItem: NSView {
    private var _image:NSImage = NSImage(named: "StatusIcon")!
    private var _alternateImage:NSImage = NSImage(named: "StatusIconWhite")!
    
    
    internal var delegate:StatusItemDelegate?
    
    
    
    func toggleWindow() {
        if let del:StatusItemDelegate = delegate {
            del.toggleWindow(self)
        }
    }
    
    override func mouseDown(theEvent: NSEvent) {
        self.toggleWindow()
        self.needsDisplay = true
    }
    
    override func drawRect(dirtyRect: NSRect) {
        super.drawRect(dirtyRect)

        let imagePosition:NSPoint = NSMakePoint(bounds.width/2 - _image.size.width/2, bounds.height/2 - _image.size.height/2)
        _image.drawAtPoint(imagePosition, fromRect: NSZeroRect, operation: .CompositeSourceOver, fraction: 1.0)
    }
    
}
